﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Level : MonoBehaviour
{

  [SerializeField] private string menuName;

  [SerializeField] private List<string> levelNames;
  // public List<string> levelNames;
  private List<string> currentGameLevelsList;
  public int levelsRemaining;
  public string currentLevel;

  void Awake()
  {
    levelsRemaining = levelNames.Count;
    UnityEngine.SceneManagement.SceneManager.LoadScene("MenuUtama");
    // NextLevel();
    // UnityEngine.SceneManagement.SceneManager.LoadScene("Level3");
    // UnityEngine.SceneManagement.SceneManager.LoadScene("Intro");
  }

  public void NextLevel()
  {
    if (currentGameLevelsList == null || currentGameLevelsList.Count == 0)
    {
      ResetLevels();
    }

    var sceneToLoad = currentGameLevelsList.FirstOrDefault(s => true);
    UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad);

    currentLevel = sceneToLoad;
    levelsRemaining = Mathf.Clamp(levelsRemaining - 1, 0, levelsRemaining);

    if (currentGameLevelsList.Count > 1)
    {
      currentGameLevelsList = currentGameLevelsList.Skip(1).ToList();
    }
  }

  public void ResetLevels()
  {
    currentGameLevelsList = levelNames;
  }

  // not working, need rework:
  // public void RestartLevel() {
  // 	var sceneToLoad = currentGameLevelsList.FirstOrDefault(s => true);
  // 	UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad);
  // }

}
