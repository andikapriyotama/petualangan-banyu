﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Kamera : MonoBehaviour
{
  public CinemachineVirtualCameraBase kameraAktif;

  void Start() { }

  void Update() { }

  public void PindahkanKamera(CinemachineVirtualCameraBase vcam)
  {
    Debug.Log("kamera dipasang");
    if (Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera.Name != vcam.Name)
    {
      Debug.Log("kamera set ke: " + vcam);
      vcam.MoveToTopOfPrioritySubqueue();
      kameraAktif = vcam;
    }
  }

  public void GoncangKamera()
  {
    kameraAktif.GetComponent<CinemachineCameraShaker>().ShakeCamera(1f);
  }

}
