﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audio : MonoBehaviour
{

  public Sound[] sounds;

  void Awake()
  {
    foreach (Sound sound in sounds)
    {
      sound.source = gameObject.AddComponent<AudioSource>();
      sound.source.clip = sound.clip;

      sound.source.volume = sound.volume;
      sound.source.pitch = sound.pitch;
      // sound.source.playOnAwake = sound.playOnAwake;
      sound.source.loop = sound.loop;
    }
  }

  public void Play(string name)
  {
    Debug.Log("Mencoba memainkan sound: " + name);
    Sound s = Array.Find(sounds, sound => sound.name == name);
    if (s == null)
    {
      Debug.Log("Sound: " + name + " gak ditemukan");
      return;
    }
    s.source.Play();
    Debug.Log("Sound: " + name + " dimainkan");
  }

  public void Stop(string name)
  {
    Sound s = Array.Find(sounds, sound => sound.name == name);
    if (s == null)
    {
      Debug.Log("Sound: " + name + " gak ditemukan");
      return;
    }
    s.source.Stop();
  }

  public void StopSemuaSound()
  {
    foreach (Sound sound in sounds)
    {
      sound.source.Stop();
    }
  }

  public void PelankanSuaraSebentar(string name)
  {
    Sound s = Array.Find(sounds, sound => sound.name == name);
    if (s == null)
    {
      Debug.Log("Sound: " + name + " gak ditemukan");
      return;
    }
    StartCoroutine(Pelankan(s));
  }

  IEnumerator Pelankan(Sound s)
  {
    s.source.volume = .4f;
    yield return new WaitForSeconds(1.5f);
    s.source.volume = 1f;
  }

}
