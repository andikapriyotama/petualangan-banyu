﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Senjata : MonoBehaviour
{
  public SpriteRenderer[] relYangAda;

  void Awake() { }

  void Update() { }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("Player"))
    {
      _pindahPosisiAcak();
      gameObject.SetActive(false);

      GM.Audio.Play("AmbilSenjata");
    }
  }

  private void _pindahPosisiAcak()
  {
    int indexRel;
    float posisiXRandom = transform.position.x;
    float posisiY = transform.position.y;

    if (relYangAda.Length > 0)
    {
      indexRel = Random.Range(0, relYangAda.Length);

      posisiXRandom = Random.Range(relYangAda[indexRel].bounds.min.x,
        relYangAda[indexRel].bounds.max.x);

      posisiY = relYangAda[indexRel].transform.position.y;
    }

    transform.position = new Vector2(posisiXRandom, posisiY);
  }

}
