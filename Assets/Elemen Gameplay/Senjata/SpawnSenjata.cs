﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSenjata : MonoBehaviour
{
  public GameObject senjata;
  public SeranganMomon seranganMomon;
  public KesehatanMomon kesehatanMomon;

  void OnEnable()
  {
    // FindObjectOfType<SistemSerangPlayer>().onSenjataTerpakai += _munculkanSenjata; // <- semua objek senjata merespon, broadcast dari SistemSerangPlayer
    seranganMomon.onSerangan += _munculkanSenjata;
    kesehatanMomon.onMati += _destroySenjata; // <- cuma dengar dari monster yang ini saja
  }

  void Update() { }

  private void _munculkanSenjata()
  {
    Debug.Log("senjata yang dispawn: " + senjata.name);

    // if (senjata.activeSelf == false)
    senjata.SetActive(true);
  }

  private void _destroySenjata()
  {
    Debug.Log("senjata yang didestroy: " + senjata.name);
    Destroy(senjata);
  }

  void OnDisable()
  {
    // FindObjectOfType<SistemSerangPlayer>().onSenjataTerpakai -= _munculkanSenjata;
    seranganMomon.onSerangan -= _munculkanSenjata;
    kesehatanMomon.onMati -= _destroySenjata;
  }

}
