﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventGameplay : MonoBehaviour
{
  [SerializeField] private Game _komponenGame;
  [SerializeField] private UIGame _UIGame;

  [Header("Objek yang diamati")]
  [SerializeField] private KesehatanPlayer _kesehatanPlayer;
  [SerializeField] private TriggerTenggelam _tenggelam;
  [SerializeField] private TriggerMenang _goal;

  void OnEnable()
  {
    _kesehatanPlayer.onMati += _handleKalah;
    _tenggelam.onTenggelam += _handleTenggelam;
    _goal.onMisiSelesai += _handleMenang;
  }

  void Start() { }

  void Update() { }

  private void _handleKalah()
  {
    _UIGame.ToggleUIGame(false);
    StartCoroutine(_respawnSetelahDelay());
  }

  private void _handleTenggelam()
  {
    _kesehatanPlayer.MatiSeketika();
  }

  private void _handleMenang()
  {
    _UIGame.ToggleUIGame(false);
    _UIGame.TogglePanelMenang(true);

    GM.Audio.Play("MusikMenang");
  }

  private IEnumerator _respawnSetelahDelay()
  {
    yield return new WaitForSeconds(1f);
    _komponenGame.objekPlayer.SetActive(false);
    _UIGame.TogglePanelKalah(true);

    GM.Audio.PelankanSuaraSebentar("MusikGameplay");
    GM.Audio.Play("MusikKalah");
  }

  void OnDisable()
  {
    _kesehatanPlayer.onMati -= _handleKalah;
    _tenggelam.onTenggelam -= _handleTenggelam;
    _goal.onMisiSelesai -= _handleMenang;
  }

}
