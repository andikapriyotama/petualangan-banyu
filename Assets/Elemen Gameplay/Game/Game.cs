﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Game : MonoBehaviour
{

  public string namaLevel = "Level 1";

  [Header("Setup")]
  public GameObject objekPlayer;
  [SerializeField] private Transform _titikSpawnPertama;
  [SerializeField] private CinemachineVirtualCameraBase _kameraDefault;

  private KesehatanMomon[] _kesehatanMomon;

  void Awake()
  {
    GM.Checkpoint.TitikSpawnTerakhir = _titikSpawnPertama.position;
  }

  void Start()
  {
    GM.Kamera.PindahkanKamera(_kameraDefault); // <-- pasang di Start working

    // Spawn player pertama kali
    objekPlayer.transform.position = _titikSpawnPertama.position;

    _musikGameplayLevel();
    GM.Audio.Play("PlayerSpawn");
  }

  void Update() { }

  public void RespawnPlayer()
  {
    // Persiapan
    objekPlayer.GetComponent<KesehatanPlayer>().ResetNyawa();
    objekPlayer.GetComponentInChildren<SpriteRenderer>().flipX = false;
    objekPlayer.GetComponent<PlatformerMotor2D>().velocity = new Vector2(0, 0);

    objekPlayer.GetComponent<Animator>().SetTrigger("respawn");
    objekPlayer.GetComponent<Animator>().ResetTrigger("respawn");

    // Teleport
    objekPlayer.transform.position = GM.Checkpoint.TitikSpawnTerakhir;

    GM.Audio.Play("PlayerSpawn");
  }

  public void ResetMomon()
  {
    _kesehatanMomon = FindObjectsOfType<KesehatanMomon>();
    foreach (KesehatanMomon kesehatanMomon in _kesehatanMomon)
      kesehatanMomon.ResetKesehatan();
  }

  private void _musikGameplayLevel()
  {
    GM.Audio.StopSemuaSound();

    switch (namaLevel)
    {
      case "Level 1":
        GM.Audio.Play("MusikLevel1");
        break;
      case "Level 2":
        GM.Audio.Play("MusikLevel2");
        break;
      case "Level 3":
        GM.Audio.Play("MusikLevel3");
        break;
    }
  }

}