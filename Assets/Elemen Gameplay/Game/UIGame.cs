﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGame : MonoBehaviour
{
  [Header("UI HUD")]
  public GameObject indikatorLevel;
  public GameObject barKesehatan;

  [Header("UI Virtual Controller")]
  public GameObject joystick;
  public GameObject tombolLompat;

  [Header("UI Menu")]
  public GameObject tombolKeluar;
  public GameObject panelMenang;
  public GameObject panelKalah;

  void Awake()
  {
    PasangIndikatorLevel();
    TogglePanelMenang(false);
    TogglePanelKalah(false);
  }

  void Update() { }

  public void PasangIndikatorLevel()
  {
    indikatorLevel.GetComponent<Text>().text = GetComponent<Game>().namaLevel;
  }

  public void TogglePanelMenang(bool aktifkan)
  {
    panelMenang.SetActive(aktifkan);
  }

  public void TogglePanelKalah(bool aktifkan)
  {
    panelKalah.SetActive(aktifkan);
  }

  public void ToggleUIGame(bool aktifkan)
  {
    indikatorLevel.SetActive(aktifkan);
    tombolKeluar.SetActive(aktifkan);
    barKesehatan.SetActive(aktifkan);
    joystick.SetActive(aktifkan);
    tombolLompat.SetActive(aktifkan);
  }

}
