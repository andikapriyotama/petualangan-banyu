﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

	public Transform[] background;
	private float[] skalaParallax;
	public float smoothing = 1f;
	
	private Transform kamera;
	private Vector3 posisiKameraTerakhir; // di frame terakhir

	void Awake () {
		kamera = Camera.main.transform;
	}

	// Use this for initialization
	void Start () {
		posisiKameraTerakhir = kamera.position;

		skalaParallax = new float[background.Length];

		for ( int i=0; i<background.Length; i++ ) {
			skalaParallax[i] = background[i].position.z * -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for ( int i=0; i<background.Length; i++ ) {
			float parallax = (posisiKameraTerakhir.x - kamera.position.x) * skalaParallax[i];

			float targetPosisiBackgroundX = background[i].position.x + parallax;

			Vector3 targetPosisiBackground = new Vector3 (
				targetPosisiBackgroundX,
				background[i].position.y,
				background[i].position.z
			);

			background[i].position = Vector3.Lerp (
				background[i].position,
				targetPosisiBackground,
				smoothing * Time.deltaTime
			);
		}

		posisiKameraTerakhir = kamera.position;
	}
}
