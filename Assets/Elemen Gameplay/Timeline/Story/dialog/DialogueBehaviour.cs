using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Cinemachine;

[Serializable]
public class DialogueBehaviour : PlayableBehaviour
{
  public string characterName;
  public string dialogueLine;

  public bool hasToPause = false;
  // public CinemachineVirtualCameraBase kameraAdegan;
  public bool isLast = false;

  private bool clipPlayed = false;
  private bool pauseScheduled = false;
  private PlayableDirector director;

  public override void OnPlayableCreate(Playable playable)
  {
    director = (playable.GetGraph().GetResolver() as PlayableDirector);
  }

  public override void ProcessFrame(Playable playable, FrameData info, object playerData)
  {
    if (!clipPlayed
      && info.weight > 0f)
    {
      GameObject.FindObjectOfType<UIDialog>().PasangDialog(characterName, dialogueLine);

      if (Application.isPlaying)
      {
        if (hasToPause)
        {
          pauseScheduled = true;
        }
      }

      clipPlayed = true;
    }
  }

  public override void OnBehaviourPause(Playable playable, FrameData info)
  {
    if (pauseScheduled)
    {
      pauseScheduled = false;
      if (isLast) GameObject.FindObjectOfType<UIDialog>().lastDialogDetected = true;
      GameObject.FindObjectOfType<Timeline>().PauseTimeline(director);
    }
    else
    {
      GameObject.FindObjectOfType<UIDialog>().TogglePanelDialog(false);
    }

    clipPlayed = false;
  }
}
