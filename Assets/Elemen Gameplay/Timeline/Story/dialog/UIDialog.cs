﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDialog : MonoBehaviour
{
  public event Action onIntroSelesai;

  public GameObject panelDialog; // Komponen Text
  public Text teksNamaKarakter, teksDialog;
  public bool lastDialogDetected = false;

  void Awake()
  {
    // GM.UI.panelDialog = panelDialog;
    // GM.UI.teksNamaKarakter = teksNamaKarakter;
    // GM.UI.teksDialog = teksDialog;
  }

  void Update()
  {
    if (GameObject.FindObjectOfType<Timeline>().gameMode == Timeline.GameMode.DialogueMoment)
    {
      if (SimpleInput.GetButtonDown("Fire1"))
      {
        GameObject.FindObjectOfType<Timeline>().ResumeTimeline();

        if (lastDialogDetected)
        {
          if (onIntroSelesai != null) onIntroSelesai();
          else Debug.Log("onStory1Selesai null");
        }
      }
    }
  }

  private bool ketikSelesai = true;

  public void PasangDialog(string namaKarakter, string isiDialog)
  {
    TogglePanelDialog(true);

    // Pasang nama karakter
    teksNamaKarakter.text = namaKarakter;

    // Pasang teks dialog dengan efek ketik
    // teksDialog.text = isiDialog;
    if (ketikSelesai)
    {
      ketikSelesai = false;

      // string kalimat = daftarKalimat.Dequeue();
      StopAllCoroutines();
      StartCoroutine(KetikKalimat(isiDialog));
    }
  }

  public void TogglePanelDialog(bool aktif)
  {
    panelDialog.SetActive(aktif);
  }

  IEnumerator KetikKalimat(string kalimat)
  {
    teksDialog.text = "";
    foreach (char huruf in kalimat.ToCharArray())
    {
      teksDialog.text += huruf;
      yield return null;
    }
    ketikSelesai = true;
  }

}
