﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class Timeline : MonoBehaviour
{
  public enum GameMode
  {
    Gameplay,
    //Cutscene,
    DialogueMoment, //waiting for input
  }

  public GameMode gameMode = GameMode.Gameplay;

  private PlayableDirector activeDirector;
  [SerializeField] private List<CinemachineVirtualCameraBase> kameraDialog = new List<CinemachineVirtualCameraBase>();

  void Awake()
  {
    // GM.Kamera.PindahkanKamera(kameraDialog);
  }

  // Update is called once per frame
  void Update() { }

  //Called by the TimeMachine Clip (of type Pause)
  public void PauseTimeline(PlayableDirector whichOne)
  {
    activeDirector = whichOne;
    activeDirector.Pause();
    gameMode = GameMode.DialogueMoment; //InputManager will be waiting for a spacebar to resume

    // UIManager.Instance.TogglePressSpacebarMessage(true);
    // activeDirector.playableGraph.GetRootPlayable(0).SetSpeed(0);
  }

  //Called by the InputManager
  public void ResumeTimeline()
  {
    GameObject.FindObjectOfType<UIDialog>().TogglePanelDialog(false);
    activeDirector.Resume();
    gameMode = GameMode.Gameplay;

    foreach (CinemachineVirtualCameraBase kamera in kameraDialog)
    {
      if (kamera.Name == Camera.main.GetComponent<CinemachineBrain>().ActiveVirtualCamera.Name)
      {
        GM.Kamera.PindahkanKamera(kamera);
      }
    }

    // UIManager.Instance.TogglePressSpacebarMessage(false);
    // activeDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
  }

}
