﻿public class StateMachine
{
  // Zona owner;
  public IState stateSekarang;

  // public StateMachine(Zona owner) { this.owner = owner; }

  public void PasangState(IState state)
  {
    if (stateSekarang != null)
      stateSekarang.Exit();

    stateSekarang = state;
    stateSekarang.Enter();
  }

  public void Update()
  {
    if (stateSekarang != null) stateSekarang.Execute();
  }
}
