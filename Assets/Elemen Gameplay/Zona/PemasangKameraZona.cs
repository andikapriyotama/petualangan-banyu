﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PemasangKameraZona : MonoBehaviour
{

  [SerializeField]
  private CinemachineVirtualCameraBase _kamera;
  [SerializeField]
  private CinemachineVirtualCameraBase _kameraSelanjutnya;

  void OnEnable()
  {
    GetComponentInChildren<TriggerZonaMonster>().onTerdeteksi += _handlePasangKamera;
    GetComponent<ZonaMonster>().monster.onMati += _handleKeKameraBebas;
  }

  // void Awake()
  // {
  //   if (GetComponentInChildren<CinemachineVirtualCameraBase>().Follow != null)
  //     GetComponentInChildren<CinemachineVirtualCameraBase>().Follow = GameObject.Find("Player").transform;
  // }

  void Update() { }

  private void _handlePasangKamera()
  {
    GM.Kamera.PindahkanKamera(_kamera);
  }

  private void _handleKeKameraBebas()
  {
    StartCoroutine(PindahKameraSetelahDelay(1f));
  }

  IEnumerator PindahKameraSetelahDelay(float delay)
  {
    yield return new WaitForSeconds(delay);
    GM.Kamera.PindahkanKamera(_kameraSelanjutnya);
  }

  void OnDisable()
  {
    GetComponentInChildren<TriggerZonaMonster>().onTerdeteksi -= _handlePasangKamera;
    GetComponent<ZonaMonster>().monster.onMati -= _handleKeKameraBebas;
  }

}
