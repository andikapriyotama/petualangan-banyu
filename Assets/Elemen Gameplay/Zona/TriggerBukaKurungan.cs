﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBukaKurungan : MonoBehaviour {

	public event Action onTakluk;

	void OnTriggerEnter2D (Collider2D kontak)
	{
		if (kontak.CompareTag("Player"))
		{
			if (onTakluk != null) onTakluk ();

			GetComponent<SpriteRenderer>().enabled = false;

			Destroy (gameObject);
		}
	}
}
