﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TriggerKameraDefault : MonoBehaviour
{
  void Start() { }

  void Update() { }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("Player"))
    {
      GM.Kamera.PindahkanKamera(GetComponentInChildren<CinemachineVirtualCameraBase>());
    }
  }
}
