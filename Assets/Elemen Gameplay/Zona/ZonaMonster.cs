﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class ZonaMonster : MonoBehaviour
{

  public int IDZona = 1;
  public bool sudahTakluk = false;
  public KesehatanMomon monster;

  void OnEnable()
  {
    GetComponentInChildren<TriggerZonaMonster>().onTerdeteksi += _tutupKurungan;
    monster.onMati += _bukaKurungan;
  }

  void Update() { }

  private void _tutupKurungan()
  {
    BoxCollider2D kurungan = GetComponentsInChildren<BoxCollider2D>()[0];

    kurungan.enabled = true;
    kurungan.GetComponent<SpriteRenderer>().enabled = true;
  }

  private void _bukaKurungan()
  {
    BoxCollider2D kurungan = GetComponentsInChildren<BoxCollider2D>()[1];
    kurungan.enabled = false;
    kurungan.GetComponent<SpriteRenderer>().enabled = false;
  }
}
