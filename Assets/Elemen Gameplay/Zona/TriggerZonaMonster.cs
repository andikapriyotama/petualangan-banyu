﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZonaMonster : MonoBehaviour {

	public event Action onTerdeteksi;

	[SerializeField]
	private Collider2D _collider;
	private bool _diDalam = false;

	void Awake ()
	{
		if (!_collider.isTrigger) _collider.isTrigger = true;
		_diDalam = false;
	}
	
	void Update () {}
	
	void OnTriggerStay2D (Collider2D kontak)
	{
		if (kontak.CompareTag("Player"))
		{
			if (_cekSudahDiDalam (kontak))
			{
				_diDalam = true;
				if (onTerdeteksi != null) onTerdeteksi();

				GetComponentInChildren<SpriteRenderer>().enabled = true;
			}
		}
	}
	
	private bool _cekSudahDiDalam (Collider2D kontak)
	{
		float setengahLebarPlayer = kontak.bounds.size.x / 2;
		float tepiKiriZona = _collider.bounds.min.x;
		float tepiKananZona = _collider.bounds.max.x;

		return !_diDalam
			&& kontak.transform.position.x > tepiKiriZona + setengahLebarPlayer
			&& kontak.transform.position.x < tepiKananZona - setengahLebarPlayer;
	}

}
