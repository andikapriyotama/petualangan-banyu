﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimasiPlayer : MonoBehaviour
{

  private Animator _animatorPlayer;
  private PlatformerMotor2D _motor;
  private KontrolGerakPlayer _kontrol;
  private KesehatanPlayer _kesehatan;

  void Awake()
  {
    _motor = GetComponent<PlatformerMotor2D>();
    _kontrol = GetComponent<KontrolGerakPlayer>();
    _animatorPlayer = GetComponentInChildren<Animator>();
    _kesehatan = GetComponent<KesehatanPlayer>();
  }

  void OnEnable()
  {
    _kontrol.onLari += HandleAnimasiLari;
    _kontrol.onBerhenti += HandleAnimasiBerhenti;
    _kontrol.onLompat += HandleAnimasiLompat;
    _kesehatan.onMati += handleAnimasiMati;

    // _motor.onJumpEnd += HandleAnimasiTurun;
  }

  void Update()
  {
    // Turun, jatuh
    _animatorPlayer.SetFloat("vTurun", _motor.velocity.y);
  }

  void HandleAnimasiLari()
  {
    _animatorPlayer.SetBool("sedangLari", true);
    // Debug.Log("sedang lari");
  }

  void HandleAnimasiBerhenti()
  {
    _animatorPlayer.SetBool("sedangLari", false);
    // Debug.Log("berhenti lari");
  }

  void HandleAnimasiLompat()
  {
    GM.Audio.Play("PlayerLompat");
    _animatorPlayer.SetTrigger("lompat");
    // Debug.Log("lompat");
  }

  void handleAnimasiMati()
  {
    GetComponent<Animator>().SetTrigger("mati");
    // GetComponent<Animator>().SetBool("mati", true);
  }

  // void HandleAnimasiTurun () { 
  // 	// _animatorPlayer.SetFloat("vTurun", _motor.velocity.y);
  // }

  void OnDisable()
  {
    _kontrol.onLari -= HandleAnimasiLari;
    _kontrol.onBerhenti -= HandleAnimasiBerhenti;
    _kontrol.onLompat -= HandleAnimasiLompat;
    _kesehatan.onMati -= handleAnimasiMati;

    // _motor.onJumpEnd -= HandleAnimasiTurun;
  }

}
