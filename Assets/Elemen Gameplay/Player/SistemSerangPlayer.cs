using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using SpriteGlow;
// using Cinemachine;

public class SistemSerangPlayer : MonoBehaviour
{
  public bool SenjataTersimpan = false;

  private PlatformerMotor2D _motor;
  private KesehatanPlayer _kesehatan;

  void Awake()
  {
    _motor = GetComponent<PlatformerMotor2D>();
    _kesehatan = GetComponent<KesehatanPlayer>();
  }

  void Update() { }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("ItemSenjata"))
    {
      SenjataTersimpan = true;
      // GetComponentInChildren<SpriteGlowEffect>().OutlineWidth = 3;
      GetComponentInChildren<SpriteGlowEffect>().enabled = true;
      // GetComponentInChildren<PostProcessVolume>().enabled = true;
    }
  }

  public bool ApakahPunyaSenjata()
  {
    return SenjataTersimpan;
  }

  public void BuangSenjata()
  {
    if (SenjataTersimpan == true)
    {
      SenjataTersimpan = false;
      // GetComponentInChildren<SpriteGlowEffect>().OutlineWidth = 0;
      GetComponentInChildren<SpriteGlowEffect>().enabled = false;
      // GetComponentInChildren<PostProcessVolume>().enabled = false;
    }
  }

  public bool ApakahSeranganBerhasil()
  {
    return SenjataTersimpan && _motor.IsFalling();
  }

}
