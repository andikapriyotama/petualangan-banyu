﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDKesehatanPlayer : MonoBehaviour {

	public Slider HUDKesehatan;

	private KesehatanPlayer _kesehatan;

	void Awake ()
	{
		_kesehatan = GetComponent<KesehatanPlayer>();
	}

	void OnEnable ()
	{
		_kesehatan.onKesehatanBerubah += PerbaruiSlidernya;
	}

	void PerbaruiSlidernya (float nilai)
	{
		HUDKesehatan.value = nilai;
	}

	void Start () {}
	
	void Update () {}

	void OnDisable ()
	{
		_kesehatan.onKesehatanBerubah -= PerbaruiSlidernya;
	}

}
