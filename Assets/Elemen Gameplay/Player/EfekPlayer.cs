﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpriteGlow;

public class EfekPlayer : MonoBehaviour
{
  public GameObject efekLompat;
  public GameObject efekMendarat;

  private PlatformerMotor2D _motor;
  private KontrolGerakPlayer _kontrol;
  private Collider2D _collider2D;
  private SpriteRenderer _sprite;

  void Awake()
  {
    _motor = GetComponent<PlatformerMotor2D>();
    _kontrol = GetComponent<KontrolGerakPlayer>();
    _collider2D = GetComponent<Collider2D>(); // butuh collider untuk set posisi spawn efek
    _sprite = GetComponentInChildren<SpriteRenderer>();
  }

  void OnEnable()
  {
    _kontrol.onLompat += HandleEfekLompat;
    _motor.onLanded += HandleEfekMendarat;
  }

  void Start()
  {
    GetComponentInChildren<SpriteGlowEffect>().enabled = false;
    // GetComponentInChildren<PostProcessVolume>().enabled = false;
  }

  void Update() { }

  void HandleEfekLompat()
  {
    Vector2 posisi = new Vector2(transform.position.x, _collider2D.bounds.min.y);
    Instantiate(efekLompat, posisi, Quaternion.identity);
  }

  void HandleEfekMendarat()
  {
    Vector2 posisi = new Vector2(transform.position.x, _collider2D.bounds.min.y);
    Instantiate(efekMendarat, posisi, Quaternion.identity);
  }

  public void ngeblink()
  {
    if (GetComponent<KesehatanPlayer>().KesehatanSekarang > 0)
    {
      StartCoroutine(ngeloopOpacity(1f));
    }
  }

  IEnumerator ngeloopOpacity(float durasi)
  {
    int siklus;
    for (siklus = 0; siklus < 5; siklus++)
    {
      _sprite.color = new Color32(255, 255, 255, 55);
      yield return new WaitForSeconds(.1f);

      _sprite.color = new Color32(255, 255, 255, 255);
      yield return new WaitForSeconds(.1f);
    }
  }

  void OnDisable()
  {
    _kontrol.onLompat -= HandleEfekLompat;
    _motor.onLanded -= HandleEfekMendarat;
  }

}
