﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KesehatanPlayer : MonoBehaviour
{
  public event Action<float> onKesehatanBerubah;
  public event Action onKritis;
  public event Action onMati;

  public float KesehatanAwal = 3;
  private float _kesehatanSekarang;
  public float KesehatanSekarang
  {
    get { return _kesehatanSekarang; }
    set
    {
      if (_kesehatanSekarang != value)
      {
        _kesehatanSekarang = Mathf.Clamp(value, 0, KesehatanAwal);
        if (onKesehatanBerubah != null) onKesehatanBerubah(_kesehatanSekarang); else Debug.Log("OnKesehatanBerubah null");
      }

      if (_kesehatanSekarang <= 0)
      {
        GM.Audio.Play("PlayerMati");

        if (onMati != null) onMati(); else Debug.Log("OnMati null");
        return;
      }

      if (_kesehatanSekarang < 2)
      {
        // if (onKritis != null) onKritis(); else Debug.Log("OnKritis null"); // Kritis();
        return;
      }
    }
  }

  void Start()
  {
    ResetNyawa();
  }

  void Update() { }

  public void ResetNyawa()
  {
    KesehatanSekarang = KesehatanAwal;
  }

  public void TerimaDamage(float besarDamage = 1)
  {
    KesehatanSekarang -= besarDamage;
  }

  public void MatiSeketika()
  {
    Debug.Log("mati seketika");
    if (onMati != null) onMati(); else Debug.Log("OnMati null");
  }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("Hazard"))
    {
      TerimaDamage();
    }
  }

}
