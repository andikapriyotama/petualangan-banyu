﻿using System;
using UnityEngine;
using Cinemachine;

[SelectionBase]
[RequireComponent(typeof(PlatformerMotor2D))]
public class KontrolGerakPlayer : MonoBehaviour
{
    public event Action onLari;
    public event Action onBerhenti;
    public event Action onLompat;

    private PlatformerMotor2D _motor;
    private SpriteRenderer _sprite;

    void Awake ()
    {
        _motor = GetComponent<PlatformerMotor2D>();
        _sprite = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Jump?
        if ( SimpleInput.GetButtonDown(PC2D.Input.JUMP) )
        {
            _motor.Jump();
            _motor.DisableRestrictedArea();

            if ( _motor.IsGrounded() && onLompat != null ) onLompat();
        }

        _motor.jumpingHeld = SimpleInput.GetButton(PC2D.Input.JUMP);

        // X axis movement
        if ( Mathf.Abs(SimpleInput.GetAxis(PC2D.Input.HORIZONTAL)) > PC2D.Globals.INPUT_THRESHOLD )
        {
            _motor.normalizedXMovement = SimpleInput.GetAxis(PC2D.Input.HORIZONTAL);

            if ( _motor.facingLeft )
                _sprite.flipX = true;
            else
                _sprite.flipX = false;
                
            if ( onLari != null ) onLari();
        }
        else
        {
            _motor.normalizedXMovement = 0;
            if ( onBerhenti != null ) onBerhenti();
        }
    }
}
