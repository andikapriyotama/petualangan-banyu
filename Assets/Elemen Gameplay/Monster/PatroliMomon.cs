﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatroliMomon : MonoBehaviour {

	public float kecepatan;
	public Transform[] waypoints;

	private PlatformerMotor2D _motor;
	private SpriteRenderer _sprite;

	private Vector2 waypointKiri;
	private Vector2 waypointKanan;

	void Awake ()
	{
		_motor = GetComponent<PlatformerMotor2D>();
		_sprite = GetComponentInChildren<SpriteRenderer>();

		waypointKiri = waypoints[0].position;
		waypointKanan = waypoints[1].position;


		// if (kecepatan > 0)
		// 	_motor.groundSpeed = kecepatan;

		// // Jalan & hadap kiri
		_motor.normalizedXMovement = -1;
		_sprite.flipX = false;
	}
	
	void Update ()
	{
		if (_sampaiUjungKiri())
			balikArah ();
			
		if (_sampaiUjungKanan())
			balikArah ();
	}

	private bool _sampaiUjungKiri()
	{
		return _motor.normalizedXMovement == -1
			&& transform.position.x <= waypointKiri.x;
	}

	private bool _sampaiUjungKanan()
	{
		return _motor.normalizedXMovement == 1
			&& transform.position.x >= waypointKanan.x;
	}

	void balikArah ()
	{
		float arah = _motor.normalizedXMovement;
		_motor.normalizedXMovement = 0;
		StartCoroutine(delayFlip(arah));
	}

	IEnumerator delayFlip (float arah)
	{
		yield return new WaitForSeconds(.5f);
		
		// flip objek
		if (_motor.facingLeft)
			transform.eulerAngles = new Vector2(0, 180);
		else
			transform.eulerAngles = new Vector2(0, 0);

		yield return new WaitForSeconds(.2f);

		// Jalan ke arah sebaliknya
		_motor.normalizedXMovement = arah * -1;
	}
}
