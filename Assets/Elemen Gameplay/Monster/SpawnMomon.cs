﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMomon : MonoBehaviour {

	public int IDMomon;

	private KesehatanMomon _kesehatan;

	void Awake ()
	{
		_kesehatan = GetComponent<KesehatanMomon>();
	}

	void Start ()
	{
		_kesehatan.onMati += HandleMomonMati;
	}
	
	void Update () {}

	// public void DestroyMomon () {}

	void HandleMomonMati ()
	{

		
	}

	void OnDestroy ()
	{
		_kesehatan.onMati -= HandleMomonMati;
	}

}
