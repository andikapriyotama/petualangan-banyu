﻿using System;
using UnityEngine;
using SpriteGlow;

public class SeranganMomon : MonoBehaviour
{
  public event Action onSerangan;

  public GameObject senjataTarget;
  private KesehatanMomon _kesehatan;

  void Awake()
  {
    _kesehatan = GetComponent<KesehatanMomon>();
  }

  void Update() { }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("Player"))
    {
      Debug.Log(name + ": mengalami kontak");
      var seranganPlayer = kontak.GetComponent<SistemSerangPlayer>();
      var kesehatanPlayer = kontak.GetComponent<KesehatanPlayer>();
      var efekPlayer = kontak.GetComponent<EfekPlayer>();

      if (seranganPlayer.ApakahPunyaSenjata())
      {
        Debug.Log("terima serangan (dengan senjata)");
        if (onSerangan != null) onSerangan(); else Debug.Log("onSerangan null");
      }

      if (seranganPlayer.ApakahSeranganBerhasil())
      {
        Debug.Log("serangan pada " + name + " berhasil");

        _kesehatan.TerimaDamage();
        seranganPlayer.BuangSenjata();

        GM.Audio.Play("PlayerSerangan");
        GM.Kamera.GoncangKamera();
      }
      else
      {
        Debug.Log("serangan pada " + name + " gagal");
        kesehatanPlayer.TerimaDamage();
        seranganPlayer.BuangSenjata();

        efekPlayer.ngeblink();
        GM.Audio.Play("PlayerDamage");
      }
    }
  }

}
