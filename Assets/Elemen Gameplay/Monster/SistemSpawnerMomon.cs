﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SistemSpawnerMomon : MonoBehaviour {

	public List<GameObject> ObjekMomon = new List<GameObject>();

	void Start ()
	{}
	
	// Update is called once per frame
	void Update ()
	{}

	public void DestroyMomon (int index)
	{
		if ( index >= ObjekMomon.Count )
		{
			Debug.Log("Momon sudah habis");
			return;
		}
		else if ( ObjekMomon.Count > 0 && ObjekMomon[index] != null )
		{
			Destroy(ObjekMomon[index]);

			Debug.Log("index target: "+index);
			Debug.Log("jumlah Momon: "+ObjekMomon.Count);

			if ( index +1 < ObjekMomon.Count )
			{
				Debug.Log(index+1);
				Debug.Log(ObjekMomon[index+1]);
				ObjekMomon[index+1].SetActive(true);
				// ObjekMomon[index+1].GetComponent<KesehatanMomon>().HUDNyawa.gameObject.SetActive(true);
			}
			else
			{
				Debug.Log("Momon sudah habis");
			}
		}
	}

}
