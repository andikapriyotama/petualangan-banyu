﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class KesehatanMomon : MonoBehaviour
{
  public event Action onMati;

  public float KesehatanAwal = 3;
  private float _kesehatanSekarang;
  public float KesehatanSekarang
  {
    get { return _kesehatanSekarang; }
    set
    {
      if (_kesehatanSekarang != value)
      {
        _kesehatanSekarang = Mathf.Clamp(value, 0, KesehatanAwal);
      }

      if (_kesehatanSekarang <= 0)
      {
        GM.Audio.Play("MonsterDikalahkan");

        if (onMati != null) onMati(); else Debug.Log("Momon: OnMati null");
        Destroy(gameObject);
        return;
      }
    }
  }

  void Start()
  {
    ResetKesehatan();
  }

  void Update() { }

  public void ResetKesehatan()
  {
    _kesehatanSekarang = KesehatanAwal;
  }

  public void TerimaDamage(float besarDamage = 1)
  {
    KesehatanSekarang -= besarDamage;
  }

}
