﻿using UnityEngine;

public class DestroyEfek : MonoBehaviour {

	private float _delayBerdasarDurasi;

	void Awake ()
	{
		_delayBerdasarDurasi = gameObject.GetComponent<ParticleSystem>().main.duration;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Destroy(gameObject, _delayBerdasarDurasi);
	}
}
