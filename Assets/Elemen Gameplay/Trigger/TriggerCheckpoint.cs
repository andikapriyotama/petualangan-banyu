﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TriggerCheckpoint : MonoBehaviour {

	public int IDCheckpoint;
	public GameObject TitikSpawn;

	void Awake () {}

	void OnTriggerEnter2D (Collider2D kontak)
	{
		if (kontak.CompareTag("Player"))
		{
			GM.Checkpoint.IDCheckpointTerakhir = IDCheckpoint;
			GM.Checkpoint.TitikSpawnTerakhir = TitikSpawn.transform.position;
		}
	}

}
