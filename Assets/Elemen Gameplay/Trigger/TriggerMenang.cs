﻿using System;
using UnityEngine;
using Cinemachine;

public class TriggerMenang : MonoBehaviour {

	public event Action onMisiSelesai;

	void Update () {}

	void OnTriggerExit2D (Collider2D kontak)
	{
		if (kontak.CompareTag("Player")
			&& kontak.transform.position.x > transform.position.x)
		{
			// Kalau belum tamat, masih ada level selanjutnya:
			if (GM.Level.levelsRemaining > 0)
			{
				// update status game: misi selesai/goal tercapai
				// ...
				
				// trigger event onMisiSelesai
				if (onMisiSelesai != null) onMisiSelesai(); else Debug.Log("onMisiSelesai null");
			}
			// Kalau tamat:
			else
			{
				Debug.Log("Tamat");
				// Story, bagian ending
			}
		}
	}
	
}
