﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerTenggelam : MonoBehaviour
{

  public event Action onTenggelam;

  [SerializeField]
  private KesehatanPlayer _player;
  // private InAudioNode _sfxTenggelam;

  void Awake()
  {
    GetComponent<BoxCollider2D>().isTrigger = true;

    // _sfxTenggelam = GameObject.Find("Audio").GetComponent<Audio>().sfxTenggelam;
  }

  void OnTriggerEnter2D(Collider2D kontak)
  {
    if (kontak.CompareTag("Player"))
    {
      Debug.Log("Player tenggelam");
      // InAudio.Play(gameObject, _sfxTenggelam);

      if (onTenggelam != null)
        onTenggelam();
    }
  }

}
