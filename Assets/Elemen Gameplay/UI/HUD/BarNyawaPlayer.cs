using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarNyawaPlayer : MonoBehaviour {

    private Slider _slider;
    private float nyawa_max;

    void Awake ()
    {
        _slider = GetComponent<Slider>();
        nyawa_max = _slider.maxValue;
    }

    void Start ()
    {
        ResetHUD();
    }

    public void ResetHUD ()
    {
        _slider.maxValue = nyawa_max;
		_slider.value = nyawa_max;
    }

    public void UpdateBar(float nyawa)
    {
        _slider.value = nyawa;
    }

    public void IndikasiKritis()
    {
        // Update warna fill slider/bar
        GetComponentInChildren<Image>().color = new Color32(185,210,127,255);
		GetComponentsInChildren<Image>()[1].color = new Color32(161,176,69,255);
    }

}