﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PerintahTombolUI : MonoBehaviour
{
  public void ToggleCreditTitle(bool aktif)
  {
    GetComponent<MenuUI>().panelMenuUtama.SetActive(!aktif);
    GetComponent<MenuUI>().panelCreditTitle.SetActive(aktif);
  }

  public void KeluarAplikasi()
  {
    Application.Quit();
  }

  public void KeLevelSelanjutnya()
  {
    // Debug.Log("Ke level selanjutnya");
    GM.Level.NextLevel();
  }

  public void KeLevelNomor(int nomorLevel)
  {
    SceneManager.LoadScene("Level" + nomorLevel);
  }

  public void Respawn()
  {
    GetComponent<UIGame>().ToggleUIGame(true);
    GetComponent<UIGame>().TogglePanelKalah(false);

    GetComponent<Game>().objekPlayer.SetActive(true);
    GetComponent<Game>().ResetMomon(); // reset kesehatan Momon
    GetComponent<Game>().RespawnPlayer();
  }


  public void KeMenuUtama()
  {
    // Debug.Log("Ke menu utama");
    SceneManager.LoadScene("MenuUtama");
  }

  public void UlangLevel()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
  }

}
