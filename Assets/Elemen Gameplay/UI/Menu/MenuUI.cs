﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUI : MonoBehaviour
{
  [Header("Setup Panel Menu Utama")]
  public GameObject panelMenuUtama;

  [Header("Setup Panel Credit Title")]
  public GameObject panelCreditTitle;

  void Awake()
  {
    // tampilan default Menu Utama: dari intro
    GetComponent<PerintahTombolUI>().ToggleCreditTitle(false);

    GM.Audio.StopSemuaSound();
    GM.Audio.Play("MusikMenuUtama");
  }

  void Start(){}

  void Update() { }

}
