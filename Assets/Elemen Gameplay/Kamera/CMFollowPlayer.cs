﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CMFollowPlayer : MonoBehaviour
{
  void Awake()
  {
    // if (GetComponent<CinemachineVirtualCameraBase>().Follow != null)
    GetComponent<CinemachineVirtualCameraBase>().Follow = GameObject.Find("Player").transform;
  }

  void Update() { }

}
