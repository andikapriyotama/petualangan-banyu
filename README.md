Source game **Petualangan Banyu** untuk keperluan dokumentasi.

Projek dibuat dengan versi **2017.4.0f1** dan perlu terinstall **Android Build Support**.

---

## Credit:

- **Game Artist**: Narindra Rendra B. - *DKV, Universitas Dian Nuswantoro, 2011*
- **Game Programmer**: Andika Priyotama D.
